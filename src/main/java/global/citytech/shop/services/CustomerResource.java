package global.citytech.shop.services;


import org.ieft.annotations.PATCH;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.InputStream;

@Path("/customers")
public interface CustomerResource {

    @POST
    @Consumes("application/xml")
    public Response createCustomer(InputStream is);

    @GET
    @Path("{id : \\d+}")
    @Produces("application/xml")
    public StreamingOutput getCustomer(@PathParam("id") int id);

    @PUT
    @Path("{id : \\d+}")
    @Consumes ("application/xml")
    public void updateCustomer(@PathParam("id") int id, InputStream is);

    @PATCH
    @Path("{id}")
    @Consumes("application/xml")
    public void patchCustomer(@PathParam("id") int id, InputStream is);

    @GET
    @Path("{first : [a-zA-Z]+}-{last:[a-zA-Z]+}")
    @Produces("application/xml")
    public StreamingOutput getCustomerFirstLast(@PathParam("first") String first,
                                                @PathParam("last") String last);
}
