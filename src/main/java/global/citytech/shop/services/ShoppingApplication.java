package global.citytech.shop.services;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ShoppingApplication extends Application {
    private Set<Object> singletons = new HashSet<>();

    public ShoppingApplication() {singletons.add(new CustomerResourceService());}

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
