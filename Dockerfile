# 2nd stage, build the runtime image
FROM registry.hub.docker.com/adoptopenjdk/openjdk11-openj9:alpine-slim
RUN apk add --no-cache redis sed bash busybox-suid && addgroup -S remitpulse && adduser -S remitpulse -G remitpulse && mkdir -p /home/remitpulse/app

# Copy the binary built in the 1st stage
COPY  ex03_1.war /home/remitpulse/app/
RUN  chown -Rf remitpulse:remitpulse /home/remitpulse/app
USER remitpulse:remitpulse
WORKDIR /home/remitpulse/app

CMD ["java", "-jar", "first_war.war"]

EXPOSE 8080